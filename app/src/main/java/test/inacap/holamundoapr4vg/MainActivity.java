package test.inacap.holamundoapr4vg;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import test.inacap.holamundoapr4vg.controller.UsuariosController;
import test.inacap.holamundoapr4vg.modelo.sqlite.HolaMundoDBContract;
import test.inacap.holamundoapr4vg.vista.FormularioActivity;
import test.inacap.holamundoapr4vg.vista.HomeActivity;
import test.inacap.holamundoapr4vg.vista.HomeDrawerActivity;

public class MainActivity extends AppCompatActivity {

    private EditText editTextUsername, editTextPassword;
    private Button buttonLogin;
    private TextView textViewPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Consultar si la sesion existe
        SharedPreferences sesiones = getSharedPreferences(HolaMundoDBContract.HolaMundoSesion.SHARED_PREFERENCES_SESION, Context.MODE_PRIVATE);
        boolean yaInicioSesion = sesiones.getBoolean(HolaMundoDBContract.HolaMundoSesion.FIELD_SESION, false);
        if(yaInicioSesion){
            // Si la persona ya inicio sesion
            // pasamos a la siguiente ventana

            Intent i = new Intent(MainActivity.this, HomeDrawerActivity.class);
            startActivity(i);
            finish();
        }

        this.editTextUsername = (EditText) findViewById(R.id.etUsername);
        this.editTextPassword = (EditText) findViewById(R.id.etPassword);

        this.buttonLogin = (Button) findViewById(R.id.btLogin);

        this.textViewPassword = (TextView) findViewById(R.id.tvPassword);

        this.buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Obtener datos
                String username = editTextUsername.getText().toString();
                String password = editTextPassword.getText().toString();

                UsuariosController controller = new UsuariosController(getApplicationContext());
                boolean login = controller.usuarioLogin(username, password);

                if(login){

                    // La persona ingreso

                    // Guardamos la sesion

                    SharedPreferences sesiones = getSharedPreferences(HolaMundoDBContract.HolaMundoSesion.SHARED_PREFERENCES_SESION, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sesiones.edit();

                    editor.putBoolean(HolaMundoDBContract.HolaMundoSesion.FIELD_SESION, true);
                    editor.commit();

                    Intent i = new Intent(MainActivity.this, HomeDrawerActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Datos incorrectos", Toast.LENGTH_SHORT).show();
                }


            }
        });

        this.textViewPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // iniciar la segunda ventana
                Intent nuevaVentana = new Intent(MainActivity.this, FormularioActivity.class);
                startActivity(nuevaVentana);
            }
        });

    }
}














