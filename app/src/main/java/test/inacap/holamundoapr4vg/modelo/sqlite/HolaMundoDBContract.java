package test.inacap.holamundoapr4vg.modelo.sqlite;

import android.provider.BaseColumns;

/**
 * Created by mitlley on 01-09-17.
 */

public class HolaMundoDBContract {
    private HolaMundoDBContract(){}

    public static class HolaMundoDBUsuarios implements BaseColumns{
        public static final String TABLE_NAME = "usuarios";
        public static final String COLUMN_NAME_USERNAME = "username";
        public static final String COLUMN_NAME_PASSWORD = "password";
    }

    public static class HolaMundoSesion {
        public static final String SHARED_PREFERENCES_SESION = "sesiones";
        public static final String FIELD_SESION = "sesion";
        public static final String FIELD_USERNAME = "username";
    }
}
