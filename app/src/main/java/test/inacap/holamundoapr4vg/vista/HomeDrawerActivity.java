package test.inacap.holamundoapr4vg.vista;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import test.inacap.holamundoapr4vg.R;

public class HomeDrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_drawer);

        //Llamamos a la barra de herramientas (barra superior)
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Boton flotante, boton que esta en la esquina inferior derecha
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        // Ponemos el boton en escucha de un click
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Mensaje temporal numero 2
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        // Componente que permite la existencia del panel lateral
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        // Llamamos al panel lateral
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        // Ponemos el panel lateral en escucha
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        // Sobreescribimos el comportamiento del boton 'atras'
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        // Si el panel lateral esta abierto
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            // Cerramos el panel lateral
            drawer.closeDrawer(GravityCompat.START);
        } else {
            // De lo contrario, actuamos de forma normal
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Manejamos la seleccion de una opcion en el
        // menu de la barra de herramientas (barra superior=


        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_logout){
            // Manejamos el cierre de sesion

        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Maneja lo que pasara cuando la persona
        // seleccione un item del panel lateral

        // Discriminando por el nombre (id) del item
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        } else if (id == R.id.nav_hola){
            // Manejamos la accion para el item "Hola Mundo!"
            Toast.makeText(getApplicationContext(), "Hola!", Toast.LENGTH_SHORT).show();
        }


        // Una vez que se ejecuta la accion, se cierra el panel lateral
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
