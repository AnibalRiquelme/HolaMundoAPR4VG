package test.inacap.holamundoapr4vg.vista;

import android.content.ContentValues;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import test.inacap.holamundoapr4vg.R;
import test.inacap.holamundoapr4vg.modelo.sqlite.HolaMundoDBContract;
import test.inacap.holamundoapr4vg.modelo.sqlite.UsuariosModel;

/**
 * Created by mitlley on 25-08-17.
 */

public class FormularioActivity extends AppCompatActivity{

    private EditText etUsername, etPassword1, etPassword2;
    private Button btRegister;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);


        this.etUsername = (EditText) findViewById(R.id.etUsername);
        this.etPassword1 = (EditText) findViewById(R.id.etPassword1);
        this.etPassword2 = (EditText) findViewById(R.id.etPassword2);

        this.btRegister = (Button) findViewById(R.id.btRegister);

        this.btRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Agregamos los datos mediante la capa Modelo
                String username = etUsername.getText().toString();
                String password = etPassword1.getText().toString();

                ContentValues usuario = new ContentValues();
                usuario.put(HolaMundoDBContract.HolaMundoDBUsuarios.COLUMN_NAME_USERNAME, username);
                usuario.put(HolaMundoDBContract.HolaMundoDBUsuarios.COLUMN_NAME_PASSWORD, password);

                UsuariosModel model = new UsuariosModel(getApplicationContext());
                model.crearUsuario(usuario);

            }
        });


    }
}







