package test.inacap.holamundoapr4vg.controller;

import android.content.ContentValues;
import android.content.Context;

import test.inacap.holamundoapr4vg.modelo.sqlite.HolaMundoDBContract;
import test.inacap.holamundoapr4vg.modelo.sqlite.UsuariosModel;

/**
 * Created by mitlley on 21-09-17.
 */

public class UsuariosController {
    private UsuariosModel model;

    public UsuariosController(Context context){
        this.model = new UsuariosModel(context);
    }

    public boolean usuarioLogin(String username, String password){
        // Pedir los datos del usuario a la BD
        ContentValues usuario = this.model.obtenerUsuarioPorUsername(username);

        if(usuario == null){
            return false;
        }

        if(password.equals(usuario.get(HolaMundoDBContract.HolaMundoDBUsuarios.COLUMN_NAME_PASSWORD))){
            return true;
        }

        return false;
    }

}














